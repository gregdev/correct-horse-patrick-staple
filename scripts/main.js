(function($) {
    'use strict';

    var $mainOutput = $('.the-output');

    var clipboard = new Clipboard('button[type="button"]', {
        text: function(trigger) {
            return $mainOutput.val();
        }
    });

    clipboard.on('success', function(e) {
        $('form').addClass('copied');
        
        gtag('event', 'Copy', {
            'event_category': 'Password'
        });

        setTimeout(function() {
            $('form').removeClass('copied');
        }, 1500);
    });

    var words = [
        'kneel', 'question', 'anger', 'noise', 'little', 'whine', 'cuddly', 'communicate', 'thing', 'word', 'bounce',
        'bait', 'squeal', 'level', 'slippery', 'fat', 'premium', 'steam', 'massive', 'monstrosity', 'diligent',
        'optimal', 'paddle', 'trace', 'sock', 'label', 'squash', 'sore', 'actually', 'mailbox', 'squish', 'fire',
        'skip', 'boring', 'fool', 'fry', 'chance', 'notice', 'ring', 'bag', 'metal', 'depend', 'lumpy', 'aware',
        'beautiful', 'hammer', 'better', 'riddle', 'divide', 'belong', 'breakable', 'voice', 'auspicious', 'blink',
        'fuel', 'shiny', 'join', 'thoughtful', 'chemical', 'banter', 'fasten', 'dad', 'electrified', 'unusual', 'tap',
        'apathetic', 'note', 'permissible', 'improve', 'permit', 'buzz', 'help', 'skate', 'impress', 'bewildered',
        'structure', 'painful', 'account', 'boorish', 'pipe', 'memory', 'tail', 'misty', 'fabulous', 'lyrical',
        'donkey', 'fresh', 'unit', 'cagey', 'work', 'plain', 'measure', 'badge', 'dull', 'murder', 'actor', 'blow',
        'ink', 'uneven', 'start', 'memorise', 'acoustics', 'hushed', 'mouth', 'box', 'thin', 'interest', 'reject',
        'undesirable', 'repulsive', 'free', 'careful', 'gamy', 'cushion', 'agreeable', 'tense', 'fearful', 'saw',
        'knowing', 'tasteless', 'gaping', 'obeisant', 'suppose', 'graceful', 'supply', 'brave', 'cellar', 'abundant',
        'spiritual', 'scarf', 'command', 'quarrelsome', 'learn', 'value', 'productive', 'aggressive', 'protective',
        'tire', 'part', 'hate', 'increase', 'flowers', 'north', 'merciful', 'discover', 'apparel', 'deserted',
        'rescue', 'silky', 'slow', 'grumpy', 'wry', 'rely', 'shop', 'mean', 'guide', 'fretful', 'receptive',
        'name', 'hand', 'allow', 'rigid', 'glass', 'vessel', 'fearless', 'smooth', 'obsolete', 'flagrant', 'mammoth',
        'taboo', 'kill', 'fair', 'delicate', 'fervent', 'unwritten', 'difficult', 'degree', 'deliver', 'fancy',
        'branch', 'nation', 'hat', 'mint', 'arrange', 'full', 'wild', 'nine', 'stormy', 'teeny', 'physical', 'appear',
        'wriggle', 'exclusive', 'gather', 'possess', 'surprise', 'face', 'plough', 'striped', 'statement', 'blush',
        'drain', 'mine', 'purring', 'lewd', 'increase', 'abusive', 'develop', 'zebra', 'frogs', 'dusty', 'verdant',
        'hope', 'best', 'last', 'connection', 'jewel', 'smell', 'button', 'salt', 'nonstop', 'press', 'grieving',
        'risk', 'trick', 'standing', 'petite', 'calm', 'wine', 'baby', 'nondescript', 'suck', 'embarrassed', 'office',
        'airport', 'exist', 'note', 'tumble', 'various', 'move', 'hateful', 'satisfy', 'drab', 'tense', 'private',
        'challenge', 'rub', 'daffy', 'continue', 'country', 'wink', 'pocket', 'first', 'labored', 'zephyr', 'melted',
        'coast', 'maddening', 'correct', 'abnormal', 'dangerous', 'use', 'power', 'tearful', 'return', 'wish', 'nasty',
        'respect', 'frighten', 'married', 'fly', 'spiders', 'step', 'alluring', 'hissing', 'dinosaurs', 'pan',
        'psychotic', 'peck', 'burst', 'callous', 'known', 'business', 'reward', 'spotless', 'industrious', 'annoyance',
        'arrogant', 'wealth', 'deserve', 'statuesque', 'huge', 'care', 'sneeze', 'pedantic', 'tricky', 'fanatical',
        'handsome', 'bent', 'cherry', 'upset', 'meeting', 'trousers', 'year', 'hose', 'scat', 'scatting', 'grub',
        'grubbing', 'money', 'silly', 'sugar', 'unicorn', 'pony', 'amorphous', 'organic', 'whale', 'alpaca',
        'despondent', 'stubborn', 'pant', 'pants', 'visceral', 'bacon'
    ];

    function getRandomWord() {
        var min = 0;
        var max = words.length;
        var rand = Math.floor(Math.random() * (max - min + 1)) + min;

        return words[rand];
    }

    // http://stackoverflow.com/a/6274398/602734
    function shuffle(array) {
        let counter = array.length;

        // While there are elements in the array
        while (counter > 0) {
            // Pick a random index
            let index = Math.floor(Math.random() * counter);

            // Decrease counter by 1
            counter--;

            // And swap the last element with it
            let temp = array[counter];
            array[counter] = array[index];
            array[index] = temp;
        }

        return array;
    }

    function capitaliseFirstLetter(text) {
        return text.charAt(0).toUpperCase() + text.slice(1);
    }

    $('form').submit(function() {
        var words = ['patrick'];

        for (let i = 0; i < parseInt($('[name="words"]').val()) - 1; i++) {
            words.push(getRandomWord());
        }

        if ($('[name="caps"]').is(':checked')) {
            for (let i = 0; i < words.length; i++) {
                words[i] = capitaliseFirstLetter(words[i]);
            }
        }

        words = shuffle(words);

        if ($('[name="numbers"]').is(':checked')) {
            var min = 1;
            var max = 99;
            words.push(Math.floor(Math.random() * (max - min + 1)) + min);
        }

        $mainOutput.text(words.join(''));
        
        gtag('event', 'Generate', {
            'event_category': 'Password'
        });

        return false;
    }).submit();

})(jQuery);